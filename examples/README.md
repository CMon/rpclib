How to run it:

. Build everything
. Run the server:
.. Change to the binary directory of the server
.. Export the library paths with: export LD_LIBRARY_PATH=../lib/:../../lib
.. Start server
. Run multiple clients, to run one:
.. Change to the binary directory of the client
.. Export the library paths with: export LD_LIBRARY_PATH=../lib/:../../lib
.. Start client

