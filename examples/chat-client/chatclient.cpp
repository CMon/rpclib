#include "chatclient.h"

#include <rpclib/client/rpcclient.h>
#include <rpclib/common/types/qlistcereal.h>

#include "ui_chatclient.h"

#include <cereal/archives/json.hpp>
#include <QJsonDocument>
#include <QJsonObject>

ChatClient::ChatClient(RPCClient * rpcClient, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ChatClient)
    , rpcClient_(rpcClient)
{
	ui->setupUi(this);

	connect(ui->pbSend, &QPushButton::clicked, this, &ChatClient::onSendClicked);

	connect(rpcClient_, &RPCClient::connected, this, [this]() {
		ui->statusbar->showMessage("Connected", 30 * 1000);
		rpcClient_->messageToServer("QList<Message> ChatService::history()", QJsonValue());
	});

	connect(rpcClient_, &RPCClient::newMessageArrived, this, &ChatClient::handleMessageFromServer);
	connect(rpcClient_, &RPCClient::error, this, [this](const QString & error) {
		ui->statusbar->showMessage(error);
	});

	rpcClient_->connectToServer();
}

ChatClient::~ChatClient()
{
	delete ui;
}

void ChatClient::onSendClicked()
{
	QJsonObject request; // should be packed into an own class but as show off this is made by hand
	request.insert("username", ui->leUser->text());
	request.insert("message", ui->leMessage->text());

	rpcClient_->messageToServer("void ChatService::sendMessage(QString,QString)", QJsonValue(request));
}

void ChatClient::handleMessageFromServer(const QVariantHash & map)
{
	const QJsonObject message = QJsonObject::fromVariantHash(map);

	if (message.contains("signature")) {
		const std::string str = QJsonDocument(message.value("data").toObject()).toJson().toStdString();
		handleCallbackResponse(message.value("signature").toString(), str);
	}

	if (message.contains("notification")) {
		const std::string str = QJsonDocument(message.value("data").toObject()).toJson().toStdString();
		handleNotificationResponse(message.value("notification").toString(), str);
	}

}

void ChatClient::handleCallbackResponse(const QString & signature, const std::string & string)
{
	std::stringstream stream;
	stream.str(string);
	cereal::JSONInputArchive archive(stream);

	if (signature == "QList<Message> ChatService::history()") {
		QList<Message> messages;
		archive(messages);
		handleHistory(messages);
	}
}

void ChatClient::handleNotificationResponse(const QString & notification, const std::string & string)
{
	std::stringstream stream;
	stream.str(string);
	cereal::JSONInputArchive archive(stream);

	if (notification == "ChatService::newMessage(Message)") {
		Message msg;
		archive(msg);
		ui->tbMessages->append(msg.toString());
	}
}

void ChatClient::handleHistory(const QList<Message> & messages)
{
	for (const Message & msg: messages) {
		ui->tbMessages->append(msg.toString());
	}
}
