#include <chat-client/chatclient.h>
#include <rpclib/client/rpcclient.h>

#include <QApplication>
#include <QHostAddress>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QScopedPointer<RPCClient> client(new RPCClient(QUrl("ws://localhost:4711")));
	QScopedPointer<ChatClient> chatClient(new ChatClient(client.data()));

	chatClient.data()->show();

	return app.exec();
}
