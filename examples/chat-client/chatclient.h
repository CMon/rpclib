#pragma once

#include <chat-common/message.h>

#include <QMainWindow>

namespace Ui {
class ChatClient;
}

class RPCClient;

class ChatClient : public QMainWindow
{
	Q_OBJECT

public:
	explicit ChatClient(RPCClient * client, QWidget *parent = nullptr);
	~ChatClient();

private:
	void onSendClicked();

private slots:
	void handleMessageFromServer(const QVariantHash & map);

private:
	void handleCallbackResponse(const QString & signature, const std::string & string);
	void handleNotificationResponse(const QString & notification, const std::string & string);

	void handleHistory(const QList<Message> & messages);

private:
	Ui::ChatClient *ui;
	RPCClient * rpcClient_;
};
