#include <chat-server/services/chat-service.h>

#include <rpclib/server/rpcserver.h>

#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QHostAddress>

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	QCommandLineOption listServiceMethods(QStringList() << "l" << "listServiceMethods", QCoreApplication::translate("main", "List methods that are exported by the services"));
	QCommandLineParser argParser;
	argParser.addHelpOption();
	argParser.addOption(listServiceMethods);
	argParser.process(app);

	// init network
	const QHostAddress listenOn = QHostAddress::Any;
	const quint16 port = 4711;

	RPCServer rpcServer(listenOn, port);
	ChatService chatService;
	chatService.registerMethods(&rpcServer);

	if (argParser.isSet(listServiceMethods)) {
		for (const QPair<QString,QString> & method: rpcServer.availableServiceMethods()) {
			qDebug() << method.first << method.second;
		}
		return 0;
	}

	if (!rpcServer.start()) {
		qCritical() << "Could not start server";
	}

	return app.exec();
}
