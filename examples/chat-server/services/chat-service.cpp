#include "chat-service.h"

#include <rpclib/common/serializehelper.h>
#include <rpclib/common/types/qlistcereal.h>
#include <rpclib/server/rpcserver.h>

#include <QJsonObject>

ChatService::ChatService(QObject * parent)
    : QObject(parent)
{
}

void ChatService::registerMethods(RPCServer * server)
{
	// connect methods
	server->addCallback("void ChatService::sendMessage(QString,QString)", [=] (const QJsonValue & request, QWebSocket * sendingSocket) {
		Q_UNUSED(sendingSocket) // could be used for filtering in sendMessage
		const QString username = request.toObject().value("username").toString();
		const QString message = request.toObject().value("message").toString();
		sendMessage(username, message);
		return QJsonObject();
	}, "Receives a message from a client and distributes it back to all clients" );

	server->addCallback("QList<Message> ChatService::history()", [=] (const QJsonValue & request, QWebSocket * sendingSocket) {
		Q_UNUSED(request)
		Q_UNUSED(sendingSocket)

		QJsonValue retval;
		CEREAL_2_DEST(retval, "lastMessages", lastMessages_);

		return retval;
	}, "sends the whole history back to the client" );

	// connect notifications
	connect(this, &ChatService::newMessage, server, [server] (const Message & message) {
		QJsonValue retval;
		CEREAL_2_DEST(retval, "message", message);

		server->sendNotification("ChatService::newMessage(Message)", retval);
	});
}

void ChatService::sendMessage(const QString & username, const QString & message)
{
	const Message msg(QDateTime::currentDateTimeUtc(), username, message);
	lastMessages_ << msg;
	emit newMessage(msg);
}
