#pragma once

#include <chat-common/message.h>

#include <QObject>

class RPCServer;

class ChatService : public QObject
{
	Q_OBJECT
public:
	ChatService(QObject *parent = nullptr);

	void registerMethods(RPCServer * server);

public: // rpc methods
	void sendMessage(const QString & username, const QString & message);

signals: // signals send to clients
	void newMessage(const Message & message);

private:
	QList<Message> lastMessages_;
};
