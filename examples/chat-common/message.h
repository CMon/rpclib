#pragma once

#include <cereal/cereal.hpp>
#include <rpclib/common/types/qstringcereal.h>
#include <rpclib/common/types/qdatetimecereal.h>

#include <QDateTime>

class Message
{
public:
	Message() {}
	Message(const QDateTime & date, const QString & username, const QString & message)
	    : date_(date), username_(username), message_(message) {}

	QString toString() const;

	QDateTime date() const;
	void setDate(QDateTime date);
	QString username() const;
	void setUsername(QString username);
	QString message() const;
	void setMessage(QString message);

	template <class Archive>
	void serialize(Archive & ar) {
		ar(cereal::make_nvp("date", date_),
		   cereal::make_nvp("username", username_),
		   cereal::make_nvp("message", message_));
	}

private:
	QDateTime date_;
	QString username_;
	QString message_;
};
CEREAL_CLASS_VERSION(Message, 1)
