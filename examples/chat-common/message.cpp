#include "message.h"

QString Message::toString() const
{
	return QString("%1  %2:  %3").arg(date_.toLocalTime().toString(Qt::ISODate)).arg(username_).arg(message_);
}

QDateTime Message::date() const
{
	return date_;
}

void Message::setDate(QDateTime date)
{
	if (date_ == date)
		return;

	date_ = date;
}

QString Message::username() const
{
	return username_;
}

void Message::setUsername(QString username)
{
	if (username_ == username)
		return;

	username_ = username;
}

QString Message::message() const
{
	return message_;
}

void Message::setMessage(QString message)
{
	if (message_ == message)
		return;

	message_ = message;
}
