#pragma once

#include <QByteArray>
#include <QObject>
#include <QUrl>
#include <QWebSocket>

class RPCClient : public QObject
{
	Q_OBJECT
public:
	RPCClient(QUrl url = QUrl(), QObject *parent = nullptr);

	void setUrl(const QUrl & url);
	bool isConnected() const;
	bool waitForConnected(int timeout = 30 * 1000);

	QString lastError() const;

	bool stayConnected() const;
	void setStayConnected(bool stayConnected);

Q_SIGNALS:
	void newMessageArrived(QVariantHash message);
	void error(QString message);
	void connected();
	void disconnected();

public Q_SLOTS:
	void connectToServer();
	void disconnectFromServer();
	bool messageToServer(const QString & signature, const QJsonValue & data);
	bool messageToServer(const QJsonObject & messageObject);
	bool messageToServer(const QByteArray & message);

private Q_SLOTS:
	void onTextMessageReceived(const QString& message);
	void onConnected();

private:
	QWebSocket webSocket_;
	QUrl serverUrl_;
	QString lastError_;
	bool stayConnected_{false};
};
