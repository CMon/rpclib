#include "rpcclient.h"

#include <QCoreApplication>
#include <QElapsedTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QThread>
#include <QTimer>

RPCClient::RPCClient(QUrl url, QObject *parent)
    : QObject(parent)
    , serverUrl_(std::move(url))
{
	connect(&webSocket_, &QWebSocket::connected, this, &RPCClient::onConnected);
	connect(&webSocket_, &QWebSocket::textMessageReceived, this, &RPCClient::onTextMessageReceived);
	connect(&webSocket_, &QWebSocket::disconnected, this, [=]() {
		if (stayConnected_) {
			QTimer::singleShot(10'000, this, &RPCClient::connectToServer);
		} else {
			emit disconnected();
		}
	});
	connect(&webSocket_, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error), this, [=](QAbstractSocket::SocketError err) {
		lastError_ = webSocket_.errorString();
		emit error(lastError_);
		qCritical() << "Got websocket error:" << webSocket_.errorString();
		switch(err) {
		case QAbstractSocket::ConnectionRefusedError:
		case QAbstractSocket::RemoteHostClosedError:
		case QAbstractSocket::SocketTimeoutError:
		case QAbstractSocket::NetworkError:
			if (stayConnected_) {
				qInfo() << "Trying to reconnect, in a few seconds...";
				QTimer::singleShot(10'000, this, &RPCClient::connectToServer);
			}
			return;
		default:
			qCritical() << "Error not handled";
		}
	});
}

void RPCClient::setUrl(const QUrl & url)
{
	serverUrl_ = url;
}

bool RPCClient::isConnected() const
{
	return webSocket_.isValid();
}

bool RPCClient::waitForConnected(const int timeout)
{
	QElapsedTimer timer;
	timer.start();
	while (!webSocket_.isValid() && timer.elapsed() < timeout) {
		QCoreApplication::processEvents();
		QThread::msleep(10);
	}

	return timer.elapsed() < timeout;
}

void RPCClient::connectToServer()
{
	if (webSocket_.isValid()) {
		return;
	}

	qInfo() << "Connecting to:" << serverUrl_;
	webSocket_.open(serverUrl_);
}

void RPCClient::disconnectFromServer()
{
	webSocket_.close();
	emit disconnected();
}

bool RPCClient::messageToServer(const QString & signature, const QJsonValue & data)
{
	QJsonObject msg;
	msg.insert("signature", signature);
	msg.insert("data", data);
	return messageToServer(msg);
}

bool RPCClient::messageToServer(const QJsonObject & messageObject)
{
	return messageToServer(QJsonDocument(messageObject).toJson(QJsonDocument::Compact));
}

bool RPCClient::messageToServer(const QByteArray & message)
{
	webSocket_.sendTextMessage(message);
	if (!webSocket_.isValid()) {
		qCritical() << "Try to send message but webSocket is not connected" << webSocket_.errorString();
		return false;
	}

	return true;
}

void RPCClient::onTextMessageReceived(const QString& message)
{
	const QJsonObject jObj = QJsonDocument::fromJson(qPrintable(message)).object();

	if (jObj.contains("error")) {
		emit error(jObj.value("error").toString());
	} else {
		emit newMessageArrived(jObj.toVariantHash());
	}
}

void RPCClient::onConnected()
{
	emit connected();
}

bool RPCClient::stayConnected() const
{
	return stayConnected_;
}

void RPCClient::setStayConnected(bool stayConnected)
{
	stayConnected_ = stayConnected;
}

QString RPCClient::lastError() const
{
	return lastError_;
}
