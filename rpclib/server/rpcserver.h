#pragma once

#include <QObject>
#include <QHostAddress>
#include <QSslConfiguration>

#include <functional>

class QWebSocket;
class QWebSocketServer;
class RPCServer : public QObject
{
	Q_OBJECT
public:
	using Callback = std::function<QJsonValue (const QJsonValue &, QWebSocket *)>;
	using CallbackStream = std::function<QJsonValue (std::stringstream &, QWebSocket *)>;
	using CallbackNothing = std::function<QJsonValue (QWebSocket *)>;

	RPCServer(const QHostAddress & listenHost, const quint16 & port, const QSslConfiguration &securitySettings = QSslConfiguration(), const QString & serverTitle = "RPC Server");
	~RPCServer() override;

	bool start();
	void stop();
	bool isRunning() const;

	bool addCallback(const QString & signature, Callback callback, const QString & description);
	bool addCallback(const QString & signature, CallbackStream callback, const QString & description);
	bool addCallback(const QString & signature, CallbackNothing noArg, const QString & description);
	void removeCallback(const QString & signature);
	bool addObserver(const QString & url, const QString & description);
	bool removeObserver(const QString & url);

	QString lastError() const;
	QList<QPair<QString, QString> > availableServiceMethods() const;

	QUrl getServerUrl() const;

public slots:
	void sendNotification(const QString & signature, const QJsonValue & data, const QList<QWebSocket *>& recipients = QList<QWebSocket*>());

signals:
	void closed();
	void clientDisconnected(QWebSocket * client);

private slots:
	void onNewConnection();
	void processTextMessage(const QString& message);
	void processBinaryMessage(const QByteArray& message);
	void socketDisconnected();

private:
	QJsonValue callMethod(const QString & signature, const QJsonValue & rawData, QWebSocket * requester);
	QString handleMessage(const QString & message, QWebSocket * requester);
	QJsonObject discovery();

private:
	QHostAddress listenHost_;
	quint16 port_;
	std::unique_ptr<QWebSocketServer> webSocketServer_;
	QList<QWebSocket*> clients_;

private:
	enum CallbackType {
		QJsonValueType,
		stdStreamType,
		noArgType
	};
	struct CallbackContainer {
		CallbackType    cbType;
		Callback        cbValue;
		CallbackStream  cbStream;
		CallbackNothing cbNothing;
	};

	QHash<QString, CallbackContainer> methods_;
	QHash<QString, QString> description_;
	QString lastError_;
};
