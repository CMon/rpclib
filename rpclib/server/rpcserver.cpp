#include "rpcserver.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QWebSocket>
#include <QWebSocketServer>
#include <sstream>

#ifndef QT_NO_DEBUG
#define INSECURE_DEBUG_ENABLED
#endif

static QString requesterToString(const QWebSocket * requester) {
	if (requester->peerAddress().protocol() == QAbstractSocket::IPv6Protocol) {
		return QString("[%1]:%2").arg(requester->peerAddress().toString()).arg(requester->peerPort());
	}

	return QString("%1:%2").arg(requester->peerAddress().toString()).arg(requester->peerPort());
}

RPCServer::RPCServer(const QHostAddress & listenHost, const quint16 & port, const QSslConfiguration& securitySettings, const QString & serverTitle)
    : listenHost_(listenHost)
    , port_(port)
{
	webSocketServer_ = std::make_unique<QWebSocketServer>(serverTitle, securitySettings.isNull() ? QWebSocketServer::NonSecureMode : QWebSocketServer::SecureMode, this);
	if(!securitySettings.isNull()) {
		webSocketServer_->setSslConfiguration(securitySettings);
	}
	addCallback("discovery", [=] (QWebSocket *) {
		return this->discovery();
	}, "Get a list of registered services and notifications");
}

RPCServer::~RPCServer()
{
	qDeleteAll(clients_);
}

bool RPCServer::start()
{
	if (webSocketServer_->listen(listenHost_, port_)) {
		qInfo() << QString("Listening on %1").arg(webSocketServer_->serverUrl().toString());
		connect(webSocketServer_.get(), &QWebSocketServer::newConnection, this, &RPCServer::onNewConnection);
		connect(webSocketServer_.get(), &QWebSocketServer::closed, this, &RPCServer::closed);
		return true;
	}
	lastError_ = webSocketServer_->errorString();
	return false;
}

void RPCServer::stop()
{
	webSocketServer_->close();
}

bool RPCServer::isRunning() const
{
	return webSocketServer_->isListening();
}

bool RPCServer::addCallback(const QString & signature, RPCServer::Callback callback, const QString & description)
{
	if (methods_.contains(signature)) {
		qWarning() << "signature:" << signature << "already registered";
		return false;
	}
	qDebug() << "registered rpc method:" << signature;


	CallbackContainer cc;
	cc.cbType = QJsonValueType;
	cc.cbValue = std::move(callback);

	methods_[signature] = cc;
	description_[signature] = description;
	return true;
}

bool RPCServer::addCallback(const QString & signature, RPCServer::CallbackStream callback, const QString & description)
{
	if (methods_.contains(signature)) {
		qWarning() << "signature:" << signature << "already registered";
		return false;
	}
	qDebug() << "registered rpc method:" << signature;


	CallbackContainer cc;
	cc.cbType = stdStreamType;
	cc.cbStream = std::move(callback);

	methods_[signature] = cc;
	description_[signature] = description;
	return true;
}

bool RPCServer::addCallback(const QString & signature, RPCServer::CallbackNothing noArg, const QString & description)
{
	if (methods_.contains(signature)) {
		qWarning() << "signature:" << signature << "already registered";
		return false;
	}
	qDebug() << "registered rpc method:" << signature;

	CallbackContainer cc;
	cc.cbType = noArgType;
	cc.cbNothing = noArg;

	methods_[signature] = cc;
	description_[signature] = description;
	return true;
}

void RPCServer::removeCallback(const QString & signature)
{
	methods_.remove(signature);
	description_.remove(signature);
}

void RPCServer::sendNotification(const QString & signature, const QJsonValue & data, const QList<QWebSocket *>& recipients)
{
	QJsonObject message;
	message.insert("notification", signature);
	message.insert("data", data);

	const QString msg = QJsonDocument(message).toJson();

	if (recipients.isEmpty()) {
		for (QWebSocket * socket: qAsConst(clients_)) {
			socket->sendTextMessage(msg);
		}
	} else {
		for (QWebSocket * socket: recipients) {
			if (!clients_.contains(socket)) {
				continue;
			}
			socket->sendTextMessage(msg);
		}
	}
}

void RPCServer::onNewConnection()
{
	QWebSocket *socket = webSocketServer_->nextPendingConnection();
	qInfo() << "New connection from:" << requesterToString(socket);

	connect(socket, &QWebSocket::textMessageReceived, this, &RPCServer::processTextMessage);
	connect(socket, &QWebSocket::binaryMessageReceived, this, &RPCServer::processBinaryMessage);
	connect(socket, &QWebSocket::disconnected, this, &RPCServer::socketDisconnected);

	clients_ << socket;
}

void RPCServer::processTextMessage(const QString& message)
{
	auto *client = qobject_cast<QWebSocket *>(sender());
	const QString answer = handleMessage(message, client);
	if (!answer.isEmpty()) {
		client->sendTextMessage(answer);
	}
}

void RPCServer::processBinaryMessage(const QByteArray& message)
{
	processTextMessage(message);
}

void RPCServer::socketDisconnected()
{
	auto * client = qobject_cast<QWebSocket *>(sender());
	if (client != nullptr && clients_.contains(client)) {
		emit clientDisconnected(client);
		clients_.removeAll(client);
		client->deleteLater();
	}
}

static QString generateMissingSignatureErrorMessage()
{
	QJsonObject root;
	root.insert("error", "signature not found");
	return QJsonDocument(root).toJson();
}

QJsonValue RPCServer::callMethod(const QString & signature, const QJsonValue & rawData, QWebSocket * requester)
{
	CallbackContainer cc = methods_[signature];

	switch (cc.cbType) {
		case RPCServer::QJsonValueType:
			return cc.cbValue(rawData, requester);
		case RPCServer::stdStreamType: {
			const std::string str = QJsonDocument(rawData.toObject()).toJson().toStdString();
			std::stringstream stream;
			stream.str(str);
			return cc.cbStream(stream, requester);
		}
		case RPCServer::noArgType:
			return cc.cbNothing(requester);
	}
	return {};
}

QString RPCServer::handleMessage(const QString & message, QWebSocket * requester)
{
	const QJsonObject request = QJsonDocument::fromJson(qPrintable(message)).object();
	const QString signature = request.value("signature").toString();
	const QJsonValue rawData = request.value("data");

	qDebug () << QString("Incoming request: %1 (from: %2)").arg(signature, requesterToString(requester));
#ifdef INSECURE_DEBUG_ENABLED
	if (!rawData.isNull()) {qDebug () << "Arguments Data:" << rawData;}
#endif

	if (!methods_.contains(signature)) {
		qWarning() << QString("Invalid signature (%1) requested (from: %2)").arg(signature, requesterToString(requester));
		return generateMissingSignatureErrorMessage();
	}

	const QJsonValue data = callMethod(signature, rawData, requester);

	if (data.isNull()) {
		return {};
	}
	QJsonObject answer;
	answer.insert("signature", signature);
	answer.insert("data", data);
	return QJsonDocument(answer).toJson(QJsonDocument::Compact);
}

QJsonObject RPCServer::discovery()
{
	QJsonObject root;

	QJsonArray signatures;
	for (const QString & sig: methods_.keys()) {
		QJsonObject jSig;
		jSig.insert("signature", sig);
		jSig.insert("description", description_.value(sig));
		signatures << jSig;
	}

	root.insert("signatures", signatures);

	return root;
}

QString RPCServer::lastError() const
{
	return lastError_;
}

QList<QPair<QString, QString> > RPCServer::availableServiceMethods() const
{
	QList<QPair<QString,QString>> retval;
	for (const QString & sig: methods_.keys()) {

		retval << qMakePair(sig, description_.value(sig));
	}
	return retval;
}

QUrl RPCServer::getServerUrl() const
{
	return  webSocketServer_->serverUrl();
}
