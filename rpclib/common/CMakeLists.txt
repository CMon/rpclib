set(LIB_NAME rpclib-common)
include(GNUInstallDirs)
set(CMAKE_AUTOMOC ON)

find_package(Qt5Core CONFIG REQUIRED)
find_package(cereal CONFIG REQUIRED)

add_library(${LIB_NAME} SHARED
    serializehelper.h
    types/qdatetimecereal.h
    types/qhashcereal.h
    types/qlistcereal.h
    types/qlocalecereal.h
    types/qsetcereal.h
    types/qstringcereal.h
    types/types.h
)
target_link_libraries(${LIB_NAME} Qt5::Core)
