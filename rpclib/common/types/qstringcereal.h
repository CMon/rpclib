#ifndef QSTRINGCEREAL_H
#define QSTRINGCEREAL_H

#include <cereal/cereal.hpp>
#include <QString>

#include <string>


template <class Archive>
std::string save_minimal(Archive const &, const QString & str)
{
	return str.toStdString();
}

template <class Archive>
void load_minimal( Archive const &, QString & str, const std::string & value)
{
	str = QString::fromStdString(value);
}

#endif // QSTRINGCEREAL_H
