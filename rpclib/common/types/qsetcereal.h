#ifndef QSETCEREAL_H
#define QSETCEREAL_H

#include <cereal/cereal.hpp>

#include <QSet>

namespace cereal
{
template <class Archive, class T> inline
void CEREAL_SAVE_FUNCTION_NAME(Archive & ar, const QSet<T> & set)
{
	ar(make_size_tag( static_cast<size_type>(set.size())));

	for(auto const & i : set) {
		ar(i);
	}
}

template <class Archive, class T> inline
void CEREAL_LOAD_FUNCTION_NAME(Archive & ar, QSet<T> & set)
{
	size_type size;
	ar(make_size_tag( size ));

	for (size_t i = 0; i < size; ++i) {
		T elem;
		ar(elem);
		set.insert(elem);
	}
}
} // namespace cereal


#endif // QSETCEREAL_H
