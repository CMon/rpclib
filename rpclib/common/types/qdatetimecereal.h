#ifndef QDATETIMECEREAL_H
#define QDATETIMECEREAL_H

#include <QDateTime>

template <class Archive>
std::string save_minimal(Archive const &, const QDateTime & dt)
{
	return dt.toString(Qt::ISODate).toStdString();
}

template <class Archive>
void load_minimal( Archive const &, QDateTime & dt, const std::string & value)
{
	dt = QDateTime::fromString(QString::fromStdString(value), Qt::ISODate);
}

#endif // QDATETIMECEREAL_H
