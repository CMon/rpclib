#ifndef TYPES_H
#define TYPES_H

#include "qdatetimecereal.h"
#include "qhashcereal.h"
#include "qlistcereal.h"
#include "qlocalecereal.h"
#include "qsetcereal.h"
#include "qstringcereal.h"

#endif // TYPES_H
