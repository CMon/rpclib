#ifndef QLOCALECEREAL_H
#define QLOCALECEREAL_H

#include <cereal/cereal.hpp>
#include <QLocale>
#include <string>

template <class Archive>
std::string save_minimal(Archive const &, const QLocale & loc)
{
	return loc.bcp47Name().toStdString();
}

template <class Archive>
void load_minimal( Archive const &, QLocale & loc, const std::string & value)
{
	loc = QLocale(QString::fromStdString(value));
}

#endif // QLOCALECEREAL_H
