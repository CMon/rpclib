#ifndef QHASHCEREAL_H
#define QHASHCEREAL_H

#include <cereal/cereal.hpp>
#include <QHash>

namespace cereal
{
template <class Archive, class K, class V> inline
void CEREAL_SAVE_FUNCTION_NAME(Archive & ar, const QHash<K, V> & hash)
{
	ar(make_size_tag( static_cast<size_type>(hash.size())));

	for (auto i = hash.constBegin(); i != hash.constEnd(); ++i) {
		ar(make_map_item(i.key(), i.value()));
	}
}

template <class Archive, class K, class V> inline
void CEREAL_LOAD_FUNCTION_NAME(Archive & ar, QHash<K, V> & hash)
{
	hash.clear();
	size_type size;
	ar(make_size_tag( size ));

	for (size_t i = 0; i < size; ++i) {
		K keyElem;
		V valueElem;

		ar(make_map_item(keyElem, valueElem));
		hash.insert(keyElem, valueElem);
	}
}
} // namespace cereal

#endif // QHASHCEREAL_H
