#ifndef QLISTCEREAL_H
#define QLISTCEREAL_H

#include <cereal/cereal.hpp>

#include <QList>

namespace cereal
{
template <class Archive, class T> inline
void CEREAL_SAVE_FUNCTION_NAME(Archive & ar, const QList<T> & list)
{
	ar(make_size_tag( static_cast<size_type>(list.size())));

	for(auto const & i : list) {
		ar(i);
	}
}

template <class Archive, class T> inline
void CEREAL_LOAD_FUNCTION_NAME(Archive & ar, QList<T> & list)
{
	size_type size;
	ar(make_size_tag( size ));

	for (size_t i = 0; i < size; ++i) {
		T elem;
		ar(elem);
		list.append(elem);
	}
}
} // namespace cereal

#endif // QLISTCEREAL_H
