#ifndef SERIALIZEHELPER_H
#define SERIALIZEHELPER_H

#include <cereal/archives/json.hpp>
#include <QJsonDocument>
#include <string>

#define CEREAL_2_DEST(dest, name, obj) \
	std::stringstream destStream; \
	{\
		cereal::JSONOutputArchive jsonOut(destStream);\
		jsonOut(cereal::make_nvp(name, obj));\
	}\
	const std::string str = destStream.str();\
	dest = QJsonDocument::fromJson(QByteArray::fromStdString(str)).object()

#endif // SERIALIZEHELPER_H
